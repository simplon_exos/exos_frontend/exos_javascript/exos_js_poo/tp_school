class School {
    constructor(name) {
        this.name = name;
        this.teachers = [];
        this.students = [];
    }

    addSchool() {
        console.log(`Welcome in ${this.name} school !`);

    }

    // addStudent() {
    // console.log(`${this.name} from the ${this.promotion} is ${this.age} year\'s old`);

    // }
    addStudent(name, age, promotion) {
        this.students.push(new Student(name, age, promotion));
        // for (const student of this.students) {
        //     console.log(`Nice to meet you ${student.name}! Welcome in ${this.name}`);
        // }
    }

    addTeacher(name, speciality) {
        this.teachers.push(new Teacher(name, speciality));


    }

    fireTeacher(index) {
        let teacherToFire = this.teachers[index];
        teacherToFire.hired = !teacherToFire.hired;

        this.teachers = this.teachers.filter(item => item.hired)

    }
    listStudent() {
        for (const student of this.students) {
            console.log(`Nice to meet you ${student.name}! Welcome in the ${student.promotion} of ${this.name}`);
        }
    }
    listTeachers() {
        for (const teacher of this.teachers) {
            console.log(`Nice to meet you ${teacher.name}! Welcome in ${this.name}, `);
        }
    }
}