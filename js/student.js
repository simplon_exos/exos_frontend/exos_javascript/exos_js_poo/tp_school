class Student {
    constructor(name, age, promotion) {
        this.name = name;
        this.age = age;
        this.promotion = promotion;
        this.graduated = false;
    }

}