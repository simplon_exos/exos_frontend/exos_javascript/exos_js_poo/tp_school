// mettre teachers & student dans la school,
// virer 1 teacher,
// créer 5 students,

// let student1 = new Student('John', '28', 'Prom 15');
// let student2 = new Student('Mary', '25', 'Prom 15');
// let student3 = new Student('Elliot', '27', 'Prom 15');
// let student4 = new Student('Tim', '29', 'Prom 14');
// let student5 = new Student('Ella', '26', 'Prom 14');

// student1.addStudent();
// student2.addStudent();
// student3.addStudent();
// student4.addStudent();
// student5.addStudent();

// créer 2 teachers,
// let teacher1 = new Teacher('Philip', 'Biologie');
// let teacher2 = new Teacher('Rosa', 'History');

// teacher1.addTeacher();
// teacher2.addTeacher();


// créer 1 school,

let school1 = new School('Quiet Valley');

school1.addSchool();
school1.addStudent('John', '28', 'Prom 15');
school1.addStudent('Mary', '25', 'Prom 15');
school1.addStudent('Elliot', '27', 'Prom 15');
school1.addStudent('Tim', '29', 'Prom 14');
school1.addStudent('Ella', '26', 'Prom 14');

school1.addTeacher('Rosa', 'History');
school1.addTeacher('Philip', 'Biologie');

school1.fireTeacher(0)

school1.listStudent();
school1.listTeachers();